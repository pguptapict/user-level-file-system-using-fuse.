/* Program : Ramdisk.c (Filesystem in User space)
*  Author  : Pushpendra Gupta
*/

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>


/* Structure Definitions*/

typedef struct file1 file;
typedef struct directory1 directory;

struct file1{
	char *name;
	int permissions;
	char * data;
	int length;
	time_t time;	
	file *next;
};

struct directory1{
	char * name;
	int permissions;
	directory *childdir;
	file * childfile;
	time_t time;
	directory *next;
};

directory* root = NULL;
static double totalmemory;
static const char * currentDir;
file* createFileNode(const char* path);
directory* makeDirectoryNode();
directory* createDirectoryNode(const char* path);
char * getLastName(char *name);
char * getDirPath(const char * path);



/* This method search a directory in the metadata and returns a pointer to the node.
*   
*  Input : root, path
*
*  Output : directory *
*/

directory * getDirectoryNode(directory* dir,const char *path){
	directory *p = dir;
	directory *q;
	if(p == NULL)
		return NULL;
	if(strcmp(path,p->name) == 0){
		return p;
	}
	
	q = getDirectoryNode(p->next,path);
	if(q!=NULL)
		return q;
	q = getDirectoryNode(p->childdir,path);
	if(q!=NULL)
		return q;
	return NULL;			
}



/* This method search a file in the metadata and returns a pointer to the node.
*   
*  Input : root, path
*
*  Output : file *
*/

file * getFileNode(directory* dir,const char *path){
	directory *p = dir;
	file *f,*ff;
	
	if(p == NULL)
		return NULL;

	f = p->childfile;
	while(f!=NULL){
		if(strcmp(path,(f->name)) == 0)
			return f;
		f = f->next;			
	}
	

	ff = getFileNode(p->next,path);
	if(ff!=NULL)
		return ff;
	ff = getFileNode(p->childdir,path);
	if(ff!=NULL)
		return ff;
	return NULL;
}



/* This method is the first method called when the fuse filesystem is mounted.
*   
*  Input : fuse_conn_info
*
*  Output : Success : NULL
*/

static void* ramdisk_init(struct fuse_conn_info *conn){
//	printf("\nInside init function\n");
	directory *d;
	d = makeDirectoryNode();
	root = d;
	return NULL;
}


/* This method creates the root(/) node of the filesystem.
*   
*  Input : void
*
*  Output : directory *
*/

directory* makeDirectoryNode(){   // Making the root node
//	fprintf(stdout,"totalmemory is %ld in makedirnode\n",totalmemory);

	directory *d;
	if(totalmemory >= sizeof(directory)){
		d = (directory *)malloc(sizeof(directory));
		totalmemory = totalmemory - sizeof(directory);
	}else{
		return NULL;
	}
	d->name = "/";
	d->permissions = 0777;
	d->childdir = NULL;
	d->childfile = NULL;
	d->time = time(0);
	d->next = NULL;
	return d;
}



/* This method is used to populate the attributes of file or directory present in the filesystem.
*   
*  Input : path, stbuf
*
*  Output : Success - 0 , Failure - failure code
*/

static int ramdisk_getattr(const char *path, struct stat *stbuf){
	
	directory *d;
	file *f;
	int ret = 0;
//	fprintf(stdout,"path is %s in getattr\n",path);
//	fprintf(stdout,"totalmemory is %ld in getattr\n",totalmemory);


	d = getDirectoryNode(root,path);
	if(d == NULL){
		//dnullflag = 1;
		f = getFileNode(root,path);
		//if(f == NULL){
		//	fnullflag = 1;
		//}
	}	
	if(d == NULL && f == NULL)
		return -ENOENT;

	
	memset(stbuf, 0, sizeof(struct stat));
	
	if (d!=NULL) {
		stbuf->st_mode = S_IFDIR | 0755;
		stbuf->st_nlink = 3;
		stbuf->st_size = sizeof(directory);
		stbuf->st_mtime = d->time;
	}else if(f!=NULL){
		stbuf->st_mode = S_IFREG | 0777;
		stbuf->st_nlink = 1;
		stbuf->st_size = f->length;
		stbuf->st_mtime = f->time;		
	}else 
		ret = -ENOENT;
	return ret;
}


/* This method reads the subdirectories and files present in a directory.
*   
*  Input : path, buf, filler, offset, fuse_file_info
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_readdir(const char *path, void *buf, fuse_fill_dir_t filler,off_t offset, struct fuse_file_info *fi){
	
	directory *d,*cd;
	file *f = NULL;
	(void) offset;
	(void) fi;
	char *lastName;

	
	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);
	//fprintf(stdout,"path is %s in readdir\n",path);
	//fprintf(stdout,"totalmemory is %ld in readdir\n",totalmemory);


	d = getDirectoryNode(root,path);
	if(d == NULL){
		return -ENOENT;
	}

	cd = d->childdir;
	f = d->childfile;	
	while(cd!=NULL){
		lastName = (char *)malloc(strlen(cd->name)+1);
		memcpy(lastName,cd->name,strlen(cd->name)+1);
		lastName = getLastName(lastName);
		filler(buf,lastName,NULL,0);
		cd = cd->next;
		//free(lastName);
	}

	
	while(f!=NULL){
		lastName = (char *)malloc(strlen(f->name)+1);
		memcpy(lastName,f->name,strlen(f->name)+1);
		lastName = getLastName(lastName);
		filler(buf,lastName,NULL,0);
		f = f->next;
		//free(lastName);
	}
	
	return 0;
}




char * getLastName(char *name){
	char *pch;
	char *lastName;
	char *lastName1;
	pch = strtok(name,"/");
	 while (pch != NULL)
  	{
    		lastName = pch;
    		pch = strtok (NULL, "/");
 	}
	lastName1 = (char *)malloc(strlen(lastName)+1);
	memcpy(lastName1,lastName,strlen(lastName)+1);
	return lastName1;
}


/* This method provides the definition of open system call.
*   
*  Input : path,fuse_file_info
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_open(const char *path, struct fuse_file_info *fi)
{
	directory *d;
	file *f;	
	//printf("path is %s in open\n",path);
	//fprintf(stdout,"totalmemory is %ld in open\n",totalmemory);

	d = getDirectoryNode(root,path);
	if(d != NULL)
		return -ENOENT;

	f = getFileNode(root,path);
	if(f == NULL)
		return -ENOENT;

	return 0;
}



/* This method provides the definition of read system call.
*   
*  Input : path, buf, size, offset, fuse_file_info
*
*  Return : Success - size of data read 
*/

static int ramdisk_read(const char *path, char *buf, size_t size, off_t offset,struct fuse_file_info *fi){

	size_t len;
	file *f;
	(void) fi;
	//printf("path is %s in read\n",path);
	//fprintf(stdout,"totalmemory is %ld in read\n",totalmemory);


	f = getFileNode(root,path);
	
	len = f->length;

	if (offset < len) {
		memcpy(buf, f->data + offset, size);
	} else
		size = 0;

	return size;
}


/* This method provides definition of mknod call.
*   
*  Input : path, mode, rdev
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_mknod(const char *path, mode_t mode, dev_t rdev){
	
	char *finalPath;
	//fprintf(stdout,"path is %s in mknod\n",path);
	//fprintf(stdout,"totalmemory is %ld in mknod\n",totalmemory);
	directory *d;
	file *f;

	finalPath = getDirPath(path);
	d = getDirectoryNode(root,finalPath);
	if(d == NULL)
		return -ENOENT;

	file *nf = createFileNode(path);
	if(nf == NULL)
		return  -ENOMEM;

	if(d->childfile == NULL){
		d->childfile = nf;
		return 0;
	}
	
	f = d->childfile;
	while(f->next != NULL){
		f = f->next;
	}
	
	f->next = nf;
	return 0;
}

/* This method creates a file node.
*   
*  Input : path
*
*  Return : file * - pointer to the newly created file node. 
*/

file* createFileNode(const char* path){

	//fprintf(stdout,"totalmemory is %ld in createFileNode\n",totalmemory);

	file *f;
	if(totalmemory >= sizeof(file)){
		f = (file *)malloc(sizeof(file));
		totalmemory = totalmemory - sizeof(file);
	}else{
		return NULL;
	}

	f->name = (char *)malloc(strlen(path)+1);
	memcpy(f->name,path,strlen(path)+1);
	f->permissions = 0777;
	f->data = NULL;
	f->length = 0;
	f->time = time(0);
	f->next = NULL;

	return f;
}


/* This method provides definition of write system call.
*   
*  Input : path, buf, size, offset, fuse_file_info
*
*  Return : Success - size of data written , Failure - failure code 
*/

static int ramdisk_write(const char *path, const char *buf, size_t size,off_t offset, struct fuse_file_info *fi){

	
	char *data;
	file *f;
	//fprintf(stdout,"path is %s in write\n",path);
	//fprintf(stdout,"totalmemory is %ld in write\n",totalmemory);

	(void) fi;
	int nsize;
	f = getFileNode(root,path);
	nsize = f->length + size;
	if(totalmemory >= size){
		data = (char *)malloc(nsize);
		totalmemory = totalmemory - size;
	}else{
		return -ENOMEM;
	}
	
	memcpy(data,f->data,f->length);
	memcpy(data+f->length,buf,size);
	f->data = data;
	f->length = nsize;
	f->time = time(0);
	return size;
}

/* This method provides definition of access call.
*   
*  Input : path, mask
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_access(const char *path, int mask)
{
	//fprintf(stdout,"path is %s in access\n",path);
	//fprintf(stdout,"totalmemory is %ld in access\n",totalmemory);

	//free((char *)currentDir);
	currentDir = (char *)malloc(strlen(path)+1);
	memcpy((char *)currentDir,path,strlen(path)+1);
	
	//currentDir = path;
	return 0;
}


/* This method provides definition of mkdir system call.
*   
*  Input : path, mode
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_mkdir(const char *path, mode_t mode)
{
	
	directory *nd,*d;
	char *finalPath;
	//fprintf(stdout,"path is %s in mkdir\n",path);
	//fprintf(stdout,"totalmemory is %ld in mkdir\n",totalmemory);

	nd = createDirectoryNode(path);	
	
	if(nd == NULL)
		return  -ENOMEM;
	
	finalPath = getDirPath(path);
	d = getDirectoryNode(root,finalPath);

	if(d == NULL)
		return -1;	
	if(d->childdir == NULL){
		d->childdir = nd;
		return 0;
	}
	
	d = d->childdir;
	while(d->next != NULL){
		d = d->next;
	}
	
	d->next = nd;


	return 0;
}


/* This method creates a directory node and return it back to the caller.
*   
*  Input : path
*
*  Return : directory * - pointer to the newly created directory node. 
*/

directory* createDirectoryNode(const char* path){

	directory *d;
	//fprintf(stdout,"totalmemory is %ld in createDirectoryNode\n",totalmemory);

	if(totalmemory >= sizeof(directory)){
		d = (directory *)malloc(sizeof(directory));
		totalmemory = totalmemory - sizeof(directory);
	}else
		return NULL;


	d->name = (char *)malloc(strlen(path)+1);
	memcpy(d->name,path,strlen(path)+1);
	d->permissions = 0777;
	d->childdir = NULL;
	d->childfile = NULL;
	d->time = time(0);
	d->next = NULL;

	return d;
}


/* This method provides definition of rmdir call.
*   
*  Input : path
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_rmdir(const char *path)
{
	
	directory *parentd,*childd=NULL,*q=NULL;
	int flag = 0;
	char *finalPath;
	//fprintf(stdout,"path is %s in rmdir\n",path);
	//fprintf(stdout,"totalmemory is %ld in rmdir\n",totalmemory);

	finalPath = getDirPath(path);
	parentd = getDirectoryNode(root,finalPath);

	if(parentd->childdir == NULL){
		return -1;
	}
	
	childd = parentd->childdir;
	
	if(strcmp(path,childd->name) == 0){
		if(childd->childdir!=NULL || childd->childfile!=NULL){
			return -ENOTEMPTY;
		}
		parentd->childdir = childd->next;
		totalmemory = totalmemory + sizeof(directory);
		free(childd);
		return 0;
	}
	
	q = childd->next;
	while(q!=NULL){
		if(strcmp(path,q->name) == 0){
			flag = 1;
			break;
		}
		q = q->next;
		childd = childd->next;
	}		


	if(flag == 1){
		if(q->childdir!=NULL || q->childfile!=NULL){
			return -ENOTEMPTY;
		}
		childd->next = q->next;
		totalmemory = totalmemory + sizeof(directory);
		free(q);
		return 0;
	}

	return -1;
}

/* This method provides definition of unlink call.
*   
*  Input : path
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_unlink(const char *path)
{
	
	//fprintf(stdout,"path is %s in unlink\n",path);
	//fprintf(stdout,"totalmemory is %ld in unlink\n",totalmemory);

	int flag = 0;
	directory *d;
	file *f,*nf;


	//char *dirPath = getDirPath(currentDir);
	char *dirPath = getDirPath(path);
	d = getDirectoryNode(root,dirPath);

	if(d == NULL){
		return -1;
	}
	f = d->childfile;
	if(strcmp(path,f->name) == 0){
		d->childfile = f->next;
		totalmemory = totalmemory + sizeof(file) + f->length;
		free(f->data);		
		free(f);
		return 0;
	}

	nf = f->next;
	while(nf != NULL){
		if(strcmp(path,nf->name) == 0){
			flag = 1;
			break;
		}
		nf = nf->next;
		f = f->next;
	}
	
	if(flag == 1){
		f->next = nf->next;
		totalmemory = totalmemory + sizeof(file) + nf->length;
		free(nf->data);		
		free(nf);
		return 0;
	}
	return -1;
}



char * getDirPath(const char * path){
	
	char *dirPath,*temp;
	int pos = 0,i=0;
	dirPath = (char *)malloc(strlen(path)+1);
	memcpy((char *)dirPath,path,strlen(path)+1);

	temp = dirPath;
	while(temp[i] != '\0'){
		if(temp[i] == '/'){
			pos = i;
		} 
		i++;
	}
	dirPath[pos] = '\0';
	if(strcmp(dirPath,"") == 0)
		dirPath = "/";
	currentDir = dirPath;
	return dirPath;

}

/* This method provides definition of rename system call.
*   
*  Input : from , to
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_rename(const char *from, const char *to){
	
	//fprintf(stdout,"from is %s and to is %s in rename\n",from,to);
	//fprintf(stdout,"totalmemory is %ld in rename\n",totalmemory);

	char *finalPath;
	directory *d, *tod,*childd,*parentd,*q;
	file *f,*targetf;
	int flag = 0;
	d = getDirectoryNode(root,from);
	if(d!=NULL){
		if(strcmp(getDirPath(from),getDirPath(to)) == 0){
			free(d->name);
			d->name= (char *)malloc(strlen(to)+1);
			memcpy(d->name,to,strlen(to)+1);
			return 0;
		}else{
			finalPath = getDirPath(from);
			parentd = getDirectoryNode(root,finalPath);
			childd = parentd->childdir;
	
			if(strcmp(from,childd->name) == 0){
				parentd->childdir = childd->next;
				q = childd;
			}else{
				q = childd->next;
				while(q!=NULL){
					if(strcmp(from,q->name) == 0){
						flag = 1;
						break;
					}
				q = q->next;
				childd = childd->next;
				}		
				if(flag == 1){
					childd->next = q->next;
				}

			}
			
			
			finalPath = getDirPath(to);
			tod = getDirectoryNode(root,finalPath);

			if(tod == NULL)
				return -1;	
			free(q->name);
			q->name= (char *)malloc(strlen(to)+1);
			memcpy(q->name,to,strlen(to)+1);
			if(tod->childdir == NULL){
				tod->childdir = q;
				q->next = NULL;
				return 0;
			}
	
			tod = tod->childdir;
			while(tod->next != NULL){
				tod = tod->next;
			}
			tod->next = q;
			q->next = NULL;
			return 0;
		}	
	}

	
	d = getDirectoryNode(root, getDirPath(from));
	if(d!=NULL){
		if(strcmp(getDirPath(from),getDirPath(to)) == 0){
			f = getFileNode(root,from);			
			free(f->name);
			f->name= (char *)malloc(strlen(to)+1);
			memcpy(f->name,to,strlen(to)+1);
			return 0;
		}else{
			f = d->childfile;
			if(f == NULL)
				return -1;

			if(strcmp(from,f->name) == 0){
				d->childfile = f->next;
				targetf = f;
			}else{
				targetf = f->next;
				while(targetf!=NULL){
					if(strcmp(from,targetf->name) == 0){
						flag = 1;
						break;
					}
				targetf = targetf->next;
				f = f->next;
				}		
				if(flag == 1){
					f->next = targetf->next;
				}

			}
		
			finalPath = getDirPath(to);
			tod = getDirectoryNode(root,finalPath);

			if(tod == NULL)
				return -1;	
			free(targetf->name);
			targetf->name= (char *)malloc(strlen(to)+1);
			memcpy(targetf->name,to,strlen(to)+1);
			if(tod->childfile == NULL){
				tod->childfile = targetf;
				targetf->next = NULL;
				return 0;
			}
	
			f = tod->childfile;
			while(f->next != NULL){
				f = f->next;
			}
			f->next = targetf;
			targetf->next = NULL;
			return 0;
		}
	}
	return -1;
}


/* This method provides definition of truncate call.
*   
*  Input : path, size
*
*  Return : Success - 0 , Failure - failure code 
*/

static int ramdisk_truncate(const char *path, off_t size)
{
	//fprintf(stdout,"path is %s in truncate\n",path);
	file *f = getFileNode(root,path);
	if(f == NULL){
		return -1;
	}	
	totalmemory = totalmemory + f->length;
	free(f->data);
	f->data = NULL;
	f->length = 0;
	return 0;
}


static int ramdisk_utimens(const char *path, const struct timespec ts[2]){
	//fprintf(stdout,"path is %s in utimens\n",path);
	return 0;
}


static struct fuse_operations ramdisk_oper = {
	.init 		= ramdisk_init,
	.getattr	= ramdisk_getattr,
	.readdir	= ramdisk_readdir,
	.open		= ramdisk_open,
	.read		= ramdisk_read,
	.mknod		= ramdisk_mknod,
	.write		= ramdisk_write,
	.access		= ramdisk_access,
	.mkdir 		= ramdisk_mkdir,
	.rmdir 		= ramdisk_rmdir,
	.unlink		= ramdisk_unlink,
	.rename 	= ramdisk_rename,
	.truncate	= ramdisk_truncate,
	.utimens	= ramdisk_utimens,
};

int main(int argc, char *argv[])
{
	if(argc<3){
		printf("\nPlease run using this format \"./ramdisk /path/to/dir size\"\n");
		exit(0);
	}
	totalmemory = atof(argv[2]);
	argv[2] = argv[3];
	argc = argc -1;
	totalmemory = totalmemory * 1000000;
	return fuse_main(argc, argv, &ramdisk_oper, NULL);
}


